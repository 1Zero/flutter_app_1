import 'package:flutter/material.dart';
import 'package:flutter_application_1/practicas/widget%20con%20estado/widget_con_estado1.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(      
      appBar: AppBar(
        title: Text('columnas con filas'),
      ),
      body: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children:<Widget> [
              Container(
                height: 40,
                width: 40,
                color: Colors.blue,
              ),
              Container(
                height: 40,
                width: 40,
                color: Colors.red,
              ),
              Container(
                height: 40,
                width: 40,
                color: Colors.yellow,
              ),
              Container(
                height: 40,
                width: 40,
                color: Colors.pink,
              ),
            ]
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children:<Widget> [
              Container(
                height: 40,
                width: 40,
                color: Colors.black,
              ),
              Container(
                height: 40,
                width: 40,
                color: Colors.brown,
              ),
              Container(
                height: 40,
                width: 40,
                color: Colors.pink,
              ),
              Container(
                height: 40,
                width: 40,
                color: Colors.amber,
              )
            ],
          ),

          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children:<Widget> [
              Container(
                height: 40,
                width: 40,
                color: Colors.blue,
              ),
              Container(
                height: 40,
                width: 40,
                color: Colors.green,
              ),
              Container(
                height: 40,
                width: 40,
                color: Colors.yellow,
              ),
              Container(
                height: 40,
                width: 40,
                color: Colors.black12,
              ),            

            ],
          ),

          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                height: 40,
                width: 40,
                color: Colors.brown,
              ),
              Container(
                height: 40,
                width: 40,
                color: Colors.red,
              ),
              Container(
                height: 40,
                width: 40,
                color: Colors.deepPurple,
              ),
              Container(
                height: 40,
                width: 40,
                color: Colors.greenAccent,
              )
            ],
          ),

          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Container(
                width: 40,
                height: 40,
                color: Colors.indigo,
              ),
              Container(
                width: 40,
                height: 40,
                color: Colors.teal,
              ),
              Container(
                height: 40,
                width: 40,
                color: Colors.deepOrange,
              ),
              Container(
                width: 40,
                height: 40,
                color: Colors.grey,
              ),
            ],
          ),

          Row(
            
            children: <Widget>[
              Container(
                width: 40,
                height: 40,
                color: Colors.orange,
              ),
              Container(
                width: 40,
                height: 40,
                color: Colors.green,
              ),
              Container(
                width: 40,
                height: 40,
                color: Colors.lightGreenAccent,
              ),
              Container(
                width: 40,
                height: 40,
                color: Colors.orangeAccent,
              ),
            ],
          )
        ],
        
      ),
    );
  }
}