import 'package:flutter/material.dart';
import 'package:flutter_application_1/main.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String name = 'gael';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: AppBar(
         title: Text('cambiar nombre con refresh'),
       ),
       floatingActionButton: FloatingActionButton(
         child: Icon(Icons.refresh),
         onPressed: CambiarNombre,
       ),
    );
  }
  void CambiarNombre(){
    setState(() {
          if (name == 'gael') {
            name = 'carlos';
            
          } else {
            name ='gael';
          }
        });
  }

}